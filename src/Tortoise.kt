package tortoise

fun main(args: Array<String>) {
    for ( e in race(720,850,70)) {
        println(e)
    }
}
// return [] if v1 >= v2
fun race(v1: Int, v2: Int, g: Int): IntArray =
        if (v1 in 1..(v2 - 1) && v2 > 0 && g > 0) {
            val d = v2 - v1
            val t : Double = g/d.toDouble()
            intArrayOf(t.toInt(), (t * 60 % 60).toInt(), (t * 60 * 60 % 60).toInt())
        } else {
            intArrayOf()
        }
